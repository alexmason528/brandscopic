object @photo

event = @photo.attachable
related_with = 'Event'
venue = nil
source_title = 'Event Media Gallery'
source_type = 'gallery'
source_url = @photo.attachable_type == 'Event' ? event_path(@photo.attachable) + '#event-photos' : ''
if @photo.attachable_type == 'FormFieldResult'
  if @photo.attachable.resultable_type == 'Activity'
    source_title = @photo.attachable.resultable.activity_type.name
    source_type = 'activity_' + @photo.attachable.resultable.activitable_type.downcase
    source_url = activity_path(@photo.attachable.resultable)
    if @photo.attachable.resultable.activitable_type == 'Event'
      event = Event.find(@photo.attachable.resultable.activitable_id)
      related_with = 'Event'
    elsif @photo.attachable.resultable.activitable_type == 'Venue'
      venue = Venue.find(@photo.attachable.resultable.activitable_id)
      related_with = 'Venue'
    end
  elsif @photo.attachable.resultable_type == 'Event'
    related_with = 'Event'
    source_title = 'Post Event Recap'
    source_type = 'per'
    source_url = event_path(@photo.attachable.resultable) + '#event-per'
    event = @photo.attachable.resultable
  end
end

if related_with == 'Event'
    venue_url = nil
    if event.place
      venue = Venue.find_by(company_id: current_company.id, place_id: event.place.id)
      venue_url = venue_path(venue) if venue.present?
    end
end

video_url = @photo.video? ? @photo.file.url(:video) : nil

attributes :id
if related_with == 'Event'
    node(:title) { event.campaign_name }
    node(:type) { @photo.video? ? 'video' : 'photo' }
    node(:date) { format_date_with_time(event.start_at, true) }
    node(:address) { event.place_name_with_location('<br>') }
    node(:status) { @photo.active? }
    node(:rating) { @photo.rating }
    node(:tags) { to_select2_tag_format(@photo.tags) }
    node(:permissions) { photo_permissions(@photo) }
    node(:source) do
      {
        title: source_title,\
        type: source_type,\
      }
    end
    node(:urls) do
      {
        campaign: campaign_path(event.campaign),\
        event: event_path(event),\
        venue: venue_url,\
        deactivate: deactivate_event_photo_path(event, @photo),\
        activate: activate_event_photo_path(event, @photo),\
        download: @photo.download_url,\
        video: video_url\
      }
    end
elsif related_with == 'Venue'
    node(:title) { source_title }
    node(:type) { @photo.video? ? 'video' : 'photo' }
    node(:date) { format_date_with_time(@photo.created_at, true) }
    node(:address) { venue.place.name_with_location() }
    node(:status) { @photo.active? }
    node(:rating) { @photo.rating }
    node(:tags) { to_select2_tag_format(@photo.tags) }
    node(:permissions) { photo_permissions(@photo) }
    node(:source) do
      {
        title: source_title,\
        type: source_type,\
      }
    end
    node(:urls) do
      {
        campaign: campaign_path(@photo.attachable.resultable.campaign_id),\
        venue: venue_url,\
        download: @photo.download_url,\
        video: video_url\
      }
    end
end
