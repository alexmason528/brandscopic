module Brandscopic
  module Utils
    class UsersImport
      attr_accessor :errors, :column_index, :options
      # Receives a file handler. This allows to send the $stdin from the
      # rake file. The file loaded in an array to allow go through
      # it more than once
      def initialize(file, options = {})
        @rows = []
        opts = { row_sep: "\n", col_sep: ',', skip_blanks: true, encoding: 'ISO-8859-1' }.merge(options)
        CSV(file, opts) do |csv|
          csv.each do |r|
            @rows.push(r)
          end
        end

        @column_index = {
          first_name: 0, last_name: 1, company: 2, role: 3, email: 4, phone_number: 5,
          street_address: 6, unit_number: 7, city: 8, state: 9, zip_code: 10, country: 11,
          areas: 12, entire_us: 13, brands: 14, campaigns: 15, teams: 16,
          event_recap_due_app: 17, event_recap_late_app: 18, event_recap_pending_approval_app: 19,
          event_recap_rejected_app: 20, new_event_team_app: 21, late_task_app: 22,
          late_team_task_app: 23, new_comment_app: 24, new_team_comment_app: 25,
          new_unassigned_team_task_app: 26, new_task_assignment_app: 27, new_campaign_app: 28
        }
      end

      def run!
        inviter = User.find(1) # Inviter is Admin User
        new_users = []
        @rows.each_with_index do |row, i|
          row_errors = []
          first_name, last_name, company, role, email, phone_number,
          street_address, unit_number, city, state, zip_code, country,
          areas, entire_us, brands, campaigns, teams,
          event_recap_due_app, event_recap_late_app, event_recap_pending_approval_app,
          event_recap_rejected_app, new_event_team_app, late_task_app,
          late_team_task_app, new_comment_app, new_team_comment_app,
          new_unassigned_team_task_app, new_task_assignment_app, new_campaign_app = split_row(row)

          # Getting the company
          if !the_company = Company.where(name: company).first
            row_errors.push(line: i + 1, error: "The company \"#{company}\" for the user with the email address \"#{email}\" don't exist.")
          end

          # Verifying user existence
          if user = User.where(["lower(users.email) = '%s'", email]).first
            if user.company_users.select { |cu| cu.company_id == the_company.id }.size > 0
              @errors.push(line: i + 1, error: "The user with the email address #{email} for company #{company} already exists.")
              next
            end if the_company.present?
          else
            # Getting the country
            if the_country = Country.find_country_by_name(country)
              # Getting the state
              if !the_state = the_country.states.select { |_k, v| v['name'] == state }.first
                row_errors.push(line: i + 1, error: "The state \"#{state}\" for the user with the email address \"#{email}\" don't exist.")
              end if state.present?
            else
              row_errors.push(line: i + 1, error: "The country \"#{country}\" for the user with the email address \"#{email}\" don't exist.")
            end if country.present?

            # Getting the role
            if !the_role = Role.where(name: role, company: the_company).first
              row_errors.push(line: i + 1, error: "The role \"#{role}\" for the user with the email address \"#{email}\" don't exist.")
            end if the_company.present?

            # Getting the areas
            the_area_ids = []
            areas.split(',').map(&:strip).each do |area|
              if !the_area = Area.where(name: area, company: the_company).first
                row_errors.push(line: i + 1, error: "The area \"#{area}\" for the user with the email address \"#{email}\" don't exist.")
              else
                the_area_ids.push(the_area.id)
              end if the_company.present?
            end if areas.present?

            # Giving access to the entire United States as a place?
            us_place = Venue.includes(:place).where('company_id = ? AND places.name = ? ', the_company.id, 'United States').references(:place).first.try(:place) if entire_us.present? && entire_us.downcase == 'yes' && the_company.present?
            the_place_ids = us_place.present? ? [us_place.id] : []

            # Getting the brands
            the_brand_ids = []
            brands.split(',').map(&:strip).each do |brand|
              if !the_brand = Brand.where(name: brand, company: the_company).first
                row_errors.push(line: i + 1, error: "The brand \"#{brand}\" for the user with the email address \"#{email}\" don't exist.")
              else
                the_brand_ids.push(the_brand.id)
              end if the_company.present?
            end if brands.present?

            # Getting the campaigns
            the_campaign_ids = []
            campaigns.split(',').map(&:strip).each do |campaign|
              if !the_campaign = Campaign.where(name: campaign, company: the_company).first
                row_errors.push(line: i + 1, error: "The campaign \"#{campaign}\" for the user with the email address \"#{email}\" don't exist.")
              else
                the_campaign_ids.push(the_campaign.id)
              end if the_company.present?
            end if campaigns.present?

            # Getting the teams
            the_team_ids = []
            teams.split(',').map(&:strip).each do |team|
              if !the_team = Team.where(name: team, company: the_company).first
                row_errors.push(line: i + 1, error: "The team \"#{team}\" for the user with the email address \"#{email}\" don't exist.")
              else
                the_team_ids.push(the_team.id)
              end if the_company.present?
            end if teams.present?

            # Notifications settings
            notifications_settings = []
            notifications_settings << 'event_recap_due_app' if event_recap_due_app.present? && event_recap_due_app.downcase == 'yes'
            notifications_settings << 'event_recap_late_app' if event_recap_late_app.present? && event_recap_late_app.downcase == 'yes'
            notifications_settings << 'event_recap_pending_approval_app' if event_recap_pending_approval_app.present? && event_recap_pending_approval_app.downcase == 'yes'
            notifications_settings << 'event_recap_rejected_app' if event_recap_rejected_app.present? && event_recap_rejected_app.downcase == 'yes'
            notifications_settings << 'new_event_team_app' if new_event_team_app.present? && new_event_team_app.downcase == 'yes'
            notifications_settings << 'late_task_app' if late_task_app.present? && late_task_app.downcase == 'yes'
            notifications_settings << 'late_team_task_app' if late_team_task_app.present? && late_team_task_app.downcase == 'yes'
            notifications_settings << 'new_comment_app' if new_comment_app.present? && new_comment_app.downcase == 'yes'
            notifications_settings << 'new_team_comment_app' if new_team_comment_app.present? && new_team_comment_app.downcase == 'yes'
            notifications_settings << 'new_unassigned_team_task_app' if new_unassigned_team_task_app.present? && new_unassigned_team_task_app.downcase == 'yes'
            notifications_settings << 'new_task_assignment_app' if new_task_assignment_app.present? && new_task_assignment_app.downcase == 'yes'
            notifications_settings << 'new_campaign_app' if new_campaign_app.present? && new_campaign_app.downcase == 'yes'

            if row_errors.empty?
              user_params = {
                first_name: first_name,
                last_name: last_name,
                email: email,
                phone_number: phone_number,
                country: the_country.alpha2,
                city: city,
                state: the_state[0],
                street_address: street_address,
                unit_number: unit_number,
                zip_code: zip_code,
                created_by_id: inviter.id,
                inviting_user: true,
                company_users_attributes: {
                  '0': {
                    company_id: the_company.id,
                    role_id: the_role.id,
                    area_ids: the_area_ids,
                    place_ids: the_place_ids,
                    brand_ids: the_brand_ids,
                    campaign_ids: the_campaign_ids,
                    team_ids: the_team_ids,
                    notifications_settings: notifications_settings
                  }
                }
              }

              if new_user = User.invite!(user_params, inviter)
                new_users.push("#{new_user.full_name} - #{new_user.email} - User ID: #{new_user.id} - Company User ID: #{new_user.company_users.order(:id).last.id}")
              end
            end
          end

          if row_errors.present?
            @errors += row_errors
            next
          end
        end

        File.open('tmp/imported_users.txt', 'w') do |f|
          f.puts(new_users)
        end if new_users.present?

        @errors.empty?
      end

      def validate
        @errors = []
        @rows.each_with_index do |row, i|
          if row[4].blank?
            @errors.push(line: i + 1, error: 'The email address should be specified.')
            next
          end

          if row[0].blank? || row[1].blank?
            @errors.push(line: i + 1, error: "The full name for the user with the email address \"#{row[4]}\" should be specified.")
            next
          end

          if row[2].blank?
            @errors.push(line: i + 1, error: "The company for the user with the email address \"#{row[4]}\" should be specified.")
            next
          end

          if row[3].blank?
            @errors.push(line: i + 1, error: "The role for the user with the email address \"#{row[4]}\" should be specified.")
            next
          end
        end
        @errors.empty?
      end

      def split_row(row)
        @column_index.map { |_k, i| row[i] }
      end

      def logger
        Rails.logger
      end
    end
  end
end
